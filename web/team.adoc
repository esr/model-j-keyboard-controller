= Who we are

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

We are buckling-spring enthusiasts determined that these wonderful
keyboards shall have a new life in this century.

The crew, in alphabetical order:

*Brandon Eremita:* Master of Model M lore.  The man behind
 https://clickykeyboards.com[clickykeyboards.com], he has probably
 restored more Ms and seen more odd variants than anyone else on the
 planet.

*Jay Maynard:* Terminal-M specialist and our QMK expert. Jay does
 system- and industrial-embedded software, knows more than any mortal
 should about ancient IBM mainframes, and is sometimes known as "Tron
 Guy".He's a QMK contributor.

*Eric S. Raymond:* Project founder/manager, documentation
 specialist. Sometimes known as "ESR", he helped found the modern
 open-source movement back in the late 1990s and is the author of
 several books including "The Art of Unix Programming".  He does a lot
 in network service daemons, DSLs, and development tools.

*Michel Schwingen:* PCB design.  Michael is the author of the
 schwingen controller in the QMK tree. He does hardware design for
 network equipment by day, also the low-level stuff like bootloaders
 and board-management microcontroller suff.

*Wendell Wilson:* Production engineering. Wendell is the man who does
 it all - hardware, software, running a successful consulting
 business, and even a YouTube channel,
 https://www.youtube.com/user/teksyndicate[Level1techs].

// end
