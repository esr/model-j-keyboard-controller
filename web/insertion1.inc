Ribbon cable insertion is the trickiest part of the installation
procedure. You may want to practice for it by inserting someting like a
playing card or a business card first - provided the business card has
a glossy finish to minimize friction wear on the connector.  Get used
to the amount of force it takes to push past the resistance of the
little spring clamps in the connectors, and what it feels like when the
card bottoms out and can't be pushed further.

It's easier to do the actual insertion you have a more sideways
rather than top-down view of the connectors; start, if possible, by
putting your keyboard pan on a sturdy box that elevates it towards eye
height.  Even six inches of lift is helpful.

Then find some kind of shim about an inch thick to sit on the pan, and
rest the plate assembly on top of that.  The purpose of elevating the
plate assembly slightly is to give your fingers room to work and
make it easier to line the ribbon cables up with the cinnectors
on the Classic; choose your elevation with these goals in mind.

Slide the PCB halfway undeneath the plate assembly so the
ribbon cables drape vertically onto the connectors.  It's not
important for the controller to be sitting on the directly on the pan at
this point; things may go easier if you place it outside the edge facing
you.


